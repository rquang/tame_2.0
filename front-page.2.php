<?php get_header(); ?>

<div class="sly-container is--home">
    <div class="frame">
        <ul class="slidee">

            <?php  
                $query = new WP_Query(array(
                    'post_type' => 'photo_stories',
                    'post_status' => 'publish',
                    'posts_per_page' => 3
                ));
                
                while ($query->have_posts()) { 
                    $query->the_post();
                    $id = get_the_ID();
                    $files = get_post_meta( $id, 'shoot_gallery', 1 );
                    $imageArray = [];

                    foreach ( (array) $files as $attachment_id => $attachment_url ) {
                        $imageArray[] = wp_get_attachment_image( $attachment_id, $img_size,"",array( "class" => "lazyload", "data-sizes" => "auto" ) );
                    }

                    $rand_keys = array_rand($imageArray,2);

                    foreach($rand_keys as $key) {
                        $randomizedArray[] = array(
                            'id' => $id,
                            'image' => $imageArray[$key],
                        );
                    }
                }
            
                shuffle($randomizedArray);
            
                // Loop through them and output an image
                foreach ( (array) $randomizedArray as $single) { $id = $single['id']; ?>

                    <li class="cell is--image">
                        <a href="<?php echo esc_url(get_permalink($id)); ?>">
                            <div class="typewriter-container is--random" style="color:<?php echo get_post_meta(($id), 'shoot_color', true ); ?>;">
                                <?php echo get_the_title($id); ?>
                            </div>
                            <?php echo $single['image']; ?>
                        </a>
                    </li>

            <?php } ?>

        </ul>
    </div>
</div>  

<?php get_footer(); ?>