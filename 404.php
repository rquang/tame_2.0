<?php get_header(); ?>

    <article class="center-container">
        <h1>page not found</h1>	
        <div class="inner-content">
            <p class="typewriter-container">Lost but waiting to be found...</p>
        </div>
    </article>

<?php get_footer(); ?>