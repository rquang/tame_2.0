<?php

// flush_rewrite_rules( false );

// POST TYPES
add_theme_support( 'post-thumbnails' ); 

register_post_type( 'photo_stories',
	array(
	  'labels' => array(
	    'name' => __( 'Photo Stories' ),
	    'singular_name' => __( 'Photo Story' )
	  ),
	  'public' => true,
	  'has_archive' => true,
	  'rewrite' => array('slug' => 'photos'),
	  'hierarchical' => false,
	  'supports' => array(
		  'title',
		  'editor',
		  'thumbnail',
	  ),
	)
);

register_post_type( 'feelings',
array(
	'labels' => array(
		'name' => __( 'Feelings' ),
		'singular_name' => __( 'Feeling' )
	),
	'public' => true,
	'has_archive' => true,
	'hierarchical' => false,
	'supports' => array(
		'title',
		'editor',
		'thumbnail',
	),
)
);

?>