<?php

/**
 * Functions
 * Require all PHP files in the /woocommerce/ directory
 */
 foreach (glob(get_template_directory() . "/functions/assets/woocommerce/*.php") as $function) {
    $function= basename($function);
    require get_template_directory() . '/functions/assets/woocommerce/' . $function;
}

// Remove each style one by one
add_filter( 'woocommerce_enqueue_styles', 'jk_dequeue_styles' );
function jk_dequeue_styles( $enqueue_styles ) {
	unset( $enqueue_styles['woocommerce-general'] );	// Remove the gloss
	unset( $enqueue_styles['woocommerce-layout'] );		// Remove the layout
	unset( $enqueue_styles['woocommerce-smallscreen'] );	// Remove the smallscreen optimisation
	return $enqueue_styles;
}

// Or just remove them all in one line
// add_filter( 'woocommerce_enqueue_styles', '__return_false' );

add_filter( 'woocommerce_product_tabs', 'tame_remove_product_review', 99);
function tame_remove_product_review($tabs) {
    unset($tabs['reviews']);
    return $tabs;
}

?>