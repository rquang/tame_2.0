<?php
// CMB2 Bootstrap

if ( file_exists( __DIR__ . '/cmb2/init.php' ) ) {
	require_once __DIR__ . '/cmb2/init.php';
  } elseif ( file_exists(  __DIR__ . '/CMB2/init.php' ) ) {
	require_once __DIR__ . '/CMB2/init.php';
  }

add_action( 'cmb2_admin_init', 'cmb2_photos_metaboxes' );

function cmb2_photos_metaboxes() {
	// Start with an underscore to hide fields from custom fields list
	$prefix = '_yourprefix_';

	$cmb = new_cmb2_box( array(
		'id'            => 'shoot_info',
		'title'         => __( 'Shoot Information', 'cmb2' ),
		'object_types'  => array( 'photo_stories', ), // Post type
		'context'       => 'normal',
		'priority'      => 'high',
		'show_names'    => true, // Show field names on the left
		// 'cmb_styles' => false, // false to disable the CMB stylesheet
		// 'closed'     => true, // Keep the metabox closed by default
	) );

	$cmb->add_field( array(
		'name'    => 'Date',
		'desc'    => 'date of the photoshoot',
		'default' => '',
		'id'      => 'shoot_date',
		'type'    => 'text',
	) );

	// $cmb->add_field( array(
	// 	'name'    => 'Photographer',
	// 	'desc'    => 'name of the photographer',
	// 	'default' => '',
	// 	'id'      => 'shoot_photographer',
	// 	'type'    => 'text',
	// ) );

	$cmb->add_field( array(
		'name'    => 'Models',
		'desc'    => 'name of the models',
		'default' => '',
		'id'      => 'shoot_models',
		'type'    => 'text',
		'repeatable'     => true,
	) );
	
	$cmb->add_field( array(
		'name'    => 'Model Instagram',
		'desc'    => 'slide in the dm',
		'default' => '',
		'id'      => 'shoot_models_insta',
		'type'    => 'text',
		'repeatable'     => true,
	) );

	$cmb->add_field( array(
		'name'    => 'Background Color',
		'id'      => 'shoot_color',
		'type'    => 'colorpicker',
		'default' => '#ffffff',
	) );

	$cmb->add_field( array(
		'name' => 'Photo Gallery',
		'desc' => '',
		'id'   => 'shoot_gallery',
		'type' => 'file_list',
		// 'preview_size' => array( 100, 100 ), // Default: array( 50, 50 )
		// 'query_args' => array( 'type' => 'image' ), // Only images attachment
		// Optional, override default text strings
		'text' => array(
			'add_upload_files_text' => 'Replacement', // default: "Add or Upload Files"
			'remove_image_text' => 'Replacement', // default: "Remove Image"
			'file_text' => 'Replacement', // default: "File:"
			'file_download_text' => 'Replacement', // default: "Download"
			'remove_text' => 'Replacement', // default: "Remove"
		),
	) );
}

add_action( 'cmb2_admin_init', 'cmb2_feelings_metaboxes' );

function cmb2_feelings_metaboxes() {
	// Start with an underscore to hide fields from custom fields list
	$prefix = '_yourprefix_';

	$cmb = new_cmb2_box( array(
		'id'            => 'feelings_extras',
		'title'         => __( 'Shoot Information', 'cmb2' ),
		'object_types'  => array( 'feelings', ), // Post type
		'context'       => 'normal',
		'priority'      => 'high',
		'show_names'    => true, // Show field names on the left
		// 'cmb_styles' => false, // false to disable the CMB stylesheet
		// 'closed'     => true, // Keep the metabox closed by default
	) );

	$cmb->add_field( array(
		'name'    => 'Color',
		'id'      => 'feelings_color',
		'type'    => 'colorpicker',
		'default' => '#ffffff',
	) );

}

function cmb2_output_file_list( $file_list_meta_key, $img_size = 'medium' ) {
	
	// Get the list of files
	$files = get_post_meta( get_the_ID(), $file_list_meta_key, 1 );
	
	// Loop through them and output an image
	foreach ( (array) $files as $attachment_id => $attachment_url ) {
		echo '<li class="cell is--image">';		
		echo wp_get_attachment_image( $attachment_id, $img_size,"",array( "class" => "lazyload", "data-sizes" => "auto" ) );
		echo '</li>';
	}
}

function cmb2_output_random_file() {

	$query = new WP_Query(array(
		'post_type' => 'photo_stories',
		'post_status' => 'publish',
		'posts_per_page' => -1
	));
	
	while ($query->have_posts()) { 
		$query->the_post();
		$id = get_the_ID();
		$files = get_post_meta( $id, 'shoot_gallery', 1 );
		$imageArray = [];
		foreach ( (array) $files as $attachment_id => $attachment_url ) {
			$imageArray[] = wp_get_attachment_image( $attachment_id, $img_size,"",array( "class" => "lazyload", "data-sizes" => "auto" ) );
		}
		$rand_keys = array_rand($imageArray,2);
		foreach($rand_keys as $key) {
			$randomizedArray[] = array(
				'id' => $id,
				'image' => $imageArray[$key],
			);
		}
	}

	shuffle($randomizedArray);

	// Loop through them and output an image
	foreach ( (array) $randomizedArray as $single) { $id = $single['id']; ?>
		<li class="cell is--image">
			<a href="<?php echo esc_url(get_permalink($id)); ?>">
				<div style="background-color:<?php echo get_post_meta(($id), 'shoot_color', true ); ?>;">
					<h2><?php echo get_the_title($id); ?></h2>
					<span class="primary-btn">See Story</span>
				</div>
				<?php echo $single['image']; ?>
			</a>
		</li>
	<?php }

}

function cmb2_output_post($post, $text) {
	if (!empty( $post )): $post_id = $post->ID; ?>
		<a href="<?php echo get_permalink($post_id) ?>">
			<?php echo get_the_post_thumbnail($post,'large',array( "class" => "lazyload") ) ?>
			<span class="btn-read primary-btn" style="background-color:<?php echo get_post_meta( $post_id, 'shoot_color', true ); ?>;"><?php echo ($text . " / " . $post->post_title) ?></span>
		</a>
	<?php endif;
}

function cmb2_output_post2($post, $text) {
	if (!empty( $post )): $post_id = $post->ID; ?>
		<a href="<?php echo get_permalink($post_id) ?>">
			<div style="background-color:<?php echo get_post_meta(($post_id), 'shoot_color', true ); ?>;">
				<h3 class="typewriter-container is--random"><?php echo $post->post_title; ?></h3>
				<span class="primary-btn"><?php echo $text; ?> Story</span>
			</div>
			<?php echo get_the_post_thumbnail($post,'large',array( "class" => "lazyload") ) ?>
		</a>
	<?php endif;
}

?>