<?php

// WIDGETS
function custom_widgets_init() {
	register_sidebar( array(
		'name' => 'Header Sidebar',
		'id' => 'header-sidebar',
		'description' => 'Appears in the header area',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget' => '</aside>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	) );
	register_sidebar( array(
		'name' => 'Social Media',
		'id' => 'social-media',
		'description' => 'Appears in the header area',
		'before_widget' => '<div>',
		'after_widget' => '</div>',
	) );
	register_sidebar( array(
		'name' => 'Footer Sidebar 1',
		'id' => 'footer-sidebar-1',
		'description' => 'Appears in the footer area',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget' => '</aside>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	) );
	register_sidebar( array(
		'name' => 'Footer Sidebar 2',
		'id' => 'footer-sidebar-2',
		'description' => 'Appears in the footer area',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget' => '</aside>',
	) );
}
add_action( 'widgets_init', 'custom_widgets_init' );

?>