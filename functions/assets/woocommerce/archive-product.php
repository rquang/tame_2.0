<?php 

// Archive product before content
remove_action( 'woocommerce_before_main_content', 'woocommerce_output_content_wrapper', 10);
remove_action( 'woocommerce_before_main_content', 'woocommerce_breadcrumb', 20);
add_action('woocommerce_before_main_content', 'tame_output_content_wrapper', 5);
function tame_output_content_wrapper() {
    echo '<div id="woo-main">';
}

// Archive product after content
remove_action( 'woocommerce_after_main_content', 'woocommerce_output_content_wrapper_end', 10);
add_action('woocommerce_after_main_content', 'tame_output_content_wrapper_end', 5);
function tame_output_content_wrapper_end() {
    echo '</div>';
}

?>