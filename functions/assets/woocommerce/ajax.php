<?php

// Add this to your theme's functions.php
function ajax_add_script_to_footer(){
    if( !is_admin() && ( is_cart() || is_woocommerce() ) ) : 
        wp_enqueue_script( 'ajax', get_template_directory_uri() . '/js/ajax.js');
    endif; 
}

add_action( 'wp_footer', 'ajax_add_script_to_footer' );

add_filter('add_to_cart_fragments', 'woocommerce_header_add_to_cart_fragment');

function woocommerce_header_add_to_cart_fragment( $fragments ) {
    global $woocommerce;
    ob_start(); 

    echo('<span class="cart-contents-count">' . $woocommerce->cart->cart_contents_count . '</span>');

    $fragments['span.cart-contents-count'] = ob_get_clean();

    return $fragments;

}

?>