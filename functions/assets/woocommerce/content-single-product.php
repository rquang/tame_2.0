<?php

    remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_rating', 10 );

    remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_meta', 40 );

    remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_related_products', 20 );

    add_filter( 'woocommerce_dropdown_variation_attribute_options_args', 'tame_filter_dropdown_args', 10 );
    
    function tame_filter_dropdown_args( $args ) {
        $args['show_option_none'] = 'Select an option';
        return $args;
    }

    add_filter( 'woocommerce_variation_option_name', 'display_price_in_variation_option_name' );
    
    function display_price_in_variation_option_name( $term ) {
        global $wpdb, $product;
    
        if ( empty( $term ) ) return $term;
        if ( empty( $product->id ) ) return $term;
    
        $result = $wpdb->get_col( "SELECT slug FROM {$wpdb->prefix}terms WHERE name = '$term'" );
    
        $term_slug = ( !empty( $result ) ) ? $result[0] : $term;
    
        $query = "SELECT postmeta.post_id AS product_id
                    FROM {$wpdb->prefix}postmeta AS postmeta
                        LEFT JOIN {$wpdb->prefix}posts AS products ON ( products.ID = postmeta.post_id )
                    WHERE postmeta.meta_key LIKE 'attribute_%'
                        AND postmeta.meta_value = '$term_slug'
                        AND products.post_parent = $product->id";
    
        $variation_id = $wpdb->get_col( $query );
    
        $parent = wp_get_post_parent_id( $variation_id[0] );
    
        if ( $parent > 0 ) {
             $_product = new WC_Product_Variation( $variation_id[0] );
             return $term . ' (' . wp_kses( woocommerce_price( $_product->get_price() ), array() ) . ')';
        }
        return $term;
    
    }

    add_filter( 'woocommerce_product_tabs', 'woo_remove_product_tabs', 98 );
    
    function woo_remove_product_tabs( $tabs ) {
    
        unset( $tabs['reviews'] ); 			// Remove the reviews tab
        unset( $tabs['additional_information'] );  	// Remove the additional information tab
    
        return $tabs;
    
    }

    add_filter( 'woocommerce_product_tabs', 'woo_new_product_tab' );
    function woo_new_product_tab( $tabs ) {
        
        // Adds the new tab
        
        $tabs['returns_tab'] = array(
            'title' 	=> __( 'Returns', 'woocommerce' ),
            'priority' 	=> 50,
            'callback' 	=> 'woo_new_product_tab_content'
        );
    
        return $tabs;
    
    }
    function woo_new_product_tab_content() {
    
        // The new tab content
    
        echo '<h2>Returns</h2>';
        echo '<p>TAME will not accept any returned merchandise without prior written communication. All items that may be authorised to be returned to TAME must be in "brand new, unworn condition" and can be done so with proof of purchase within 14 days from purchase date. <b>All sale items and discounted merchandise are FINAL SALE, no exceptions.</b></p>';
        
    }
?>