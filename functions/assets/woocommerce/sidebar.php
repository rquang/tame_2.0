<?php

remove_action( 'woocommerce_sidebar', 'woocommerce_get_sidebar', 10);
add_action( 'woocommerce_sidebar', 'tame_output_sidebar_wrapper', 5);
add_action( 'woocommerce_sidebar', 'tame_get_categories', 10);
add_action( 'woocommerce_sidebar', 'tame_catalog_ordering_header', 15);
add_action( 'woocommerce_sidebar', 'woocommerce_catalog_ordering', 20);
add_action( "woocommerce_sidebar", "tame_shop_links", 25 );
add_action( 'woocommerce_sidebar', 'tame_output_sidebar_wrapper_end', 30);

function tame_output_sidebar_wrapper() {
    echo '<div id="woo-sidebar">'; ?>
    
    <header class="woocommerce-products-header">
    
                <a href="/" class="logo">
                    <img class="lazyload" src="<?php echo get_template_directory_uri(); ?>/images/tame-logo.svg" alt="Think About Me Ever Logo">
                </a>

    </header>

<?php   
}

function tame_output_sidebar_wrapper_end() {
    echo '</div>';
}

function tame_get_categories() {

    $args = array(
        'taxonomy'   => "product_cat",
        'number'     => $number,
        'orderby'    => 'title',
        'order'      => 'ASC',
        'hide_empty' => $hide_empty,
        'include'    => $ids
    );
    $product_categories = get_terms($args); ?>

    <h4>Shop /</h4>
    
    <ul class="product-cats">
    
        <li><a href="/shop/">All</a></li>

    <?php if ( $product_categories ) { ?>

            <?php foreach ( $product_categories as $term ) {
                                
                echo '<li class="category">';                                 
                    echo '<a href="' .  esc_url( get_term_link( $term ) ) . '" class="' . $term->slug . '">';
                        echo $term->name;
                    echo '</a>';
                echo '</li>';
                                                                            
            } 

    }

    echo '</ul>';
    
}

function tame_catalog_ordering_header() {
    echo '<h4>Sort By /</h4>';
}

function tame_catalog_orderby( $orderby ) {
	$orderby["menu_order"] = __('Default', 'woocommerce');
	$orderby["popularity"] = __('Best Selling', 'woocommerce');
	unset($orderby["rating"]);
	$orderby["date"] = __('Newest', 'woocommerce');
	$orderby["price"] = __('Lowest Price', 'woocommerce');
	$orderby["price-desc"] = __('Highest Price', 'woocommerce');
	return $orderby;
}
add_filter( "woocommerce_catalog_orderby", "tame_catalog_orderby", 20 );

function tame_shop_links() {
    $page = get_page_by_title( 'Shop' );
    $posts = get_children(array(
        'post_parent' => $page->ID,
        'post_type' => 'page',
        'post_status' => 'publish',
    ));

    echo '<h4>Customer Service /</h4>';
    echo '<ul>';
    foreach( $posts as $id ) {
        echo ('<li><a href="' . get_permalink($id) . '">'. get_the_title($id) . '</a>'.'</li>');
    }
    echo '</ul>';    
}

?>