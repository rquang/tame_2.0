<?php

add_action( 'wp_ajax_nopriv_random_photo_stories_ajax', 'random_photo_stories_ajax' );
add_action( 'wp_ajax_random_photo_stories_ajax', 'random_photo_stories_ajax' );

function random_photo_stories_ajax() {
    ob_start();
    random_photo_stories();
    $htmlContent = ob_get_clean();
    echo $htmlContent;
    exit;
}

function random_photo_stories() {

    $query = new WP_Query(array(
        'post_type' => 'photo_stories',
        'post_status' => 'publish',
        'posts_per_page' => -1
    ));

    while ($query->have_posts()) { 
        $query->the_post();
        $id = get_the_ID();
        $files = get_post_meta( $id, 'shoot_gallery', 1 );
        $imageArray = [];

        foreach ( (array) $files as $attachment_id => $attachment_url ) {
            $imageArray[] = wp_get_attachment_image( $attachment_id, $img_size,"",array( "class" => "lazyload", "data-sizes" => "auto" ) );
        }

        $randomizedArray[] = array(
            'id' => $id,
            'image' => $imageArray[array_rand($imageArray)]
        );
    }

    shuffle($randomizedArray);

    $rand_keys = array_rand($randomizedArray,3);

    foreach($rand_keys as $key) {
        $newRandomizedArray[] = $randomizedArray[$key];
    }

    // Loop through them and output an image
    foreach ( (array) $newRandomizedArray as $single) { $id = $single['id']; ?>

        <li class="cell is--image">
            <a href="<?php echo esc_url(get_permalink($id)); ?>">
                <div style="background-color:<?php echo get_post_meta(($id), 'shoot_color', true ); ?>;">
                    <h2 class="typewriter-container"><?php echo get_the_title($id); ?></h2>
                    <span class="primary-btn">See Story</span>
                </div>
                <?php echo $single['image']; ?>
            </a>
        </li>

    <?php } 
}

?>