<?php

// LOAD STYLESHEET
// wp_enqueue_style( 'theme-styles', get_stylesheet_directory_uri() . '/style.css', array(), filemtime( get_stylesheet_directory() . '/style.css' ) );

function theme_styles(){
    wp_enqueue_style(
        'theme-styles', 
        get_stylesheet_directory_uri() . '/style.css', 
        array(), 
        filemtime( get_stylesheet_directory() . '/style.css' ),
        'all'
    );
}
add_action( 'wp_enqueue_scripts', 'theme_styles' );

// LOAD SCRIPTS
function theme_scripts() {
    global $post;

	wp_enqueue_script( 'jquery' );
	wp_enqueue_script( 'modernizr', get_template_directory_uri() . '/js/min/modernizr.min.js');
	wp_enqueue_script( 'lazysizes', get_template_directory_uri() . '/js/min/lazysizes.min.js');
	wp_enqueue_script( 'jqueryVisible', get_template_directory_uri() . '/js/min/jquery.visible.min.js');
	wp_enqueue_script( 'sly', get_template_directory_uri() . '/js/min/sly.min.js', array( 'jquery' ), null);
	wp_enqueue_script( 'easing','https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.4.1/jquery.easing.min.js');
	
	if( !is_woocommerce() ) {
		wp_enqueue_script( 'typewriter', get_template_directory_uri() . '/js/min/typewriter.min.js');		
	}

	// wp_enqueue_script( 'main', get_template_directory_uri() . '/js/min/main.min.js',  array(), filemtime( get_template_directory_uri() . '/js/min/main.min.js' ));	

	wp_enqueue_script( 'main', get_template_directory_uri() . '/js/main.js');	
}
add_action( 'wp_enqueue_scripts', 'theme_scripts' );

?>