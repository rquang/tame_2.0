<?php

// SOCIAL SHARING 
function social_sharing_buttons() {
	global $post;
	// if(is_singular() || is_home()){
	
		// Get current page URL 
		$URL = urlencode(get_permalink());
 
		// Get current page title
		$title = str_replace( ' ', '%20', get_the_title());
		
		// Get Post Thumbnail for pinterest
		$thumbnail = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full' );
 
		// Construct sharing URL without using any script
		$twitterURL = 'https://twitter.com/intent/tweet?text='.$title.'&amp;url='.$URL.'&amp;via=TAME';
		$facebookURL = 'https://www.facebook.com/sharer/sharer.php?u='.$URL;
		$googleURL = 'https://plus.google.com/share?url='.$URL;
		$pinterestURL = 'https://pinterest.com/pin/create/button/?url='.$URL.'&amp;media='.$thumbnail[0].'&amp;description='.$title;
		$tumblrURL = 'https://www.tumblr.com/share/link?url='.$URL;
		
		$variable .= '<div id="social" class="fixed left">';
		$variable .= '<ul class="social-wrapper">';
		$variable .= '<li><a class="social-link facebook" href="'.$facebookURL.'" target="_blank">Facebook</a>';		
		$variable .= '<li><a class="social-link twitter" href="'. $twitterURL .'" target="_blank">Twitter</a>';
		$variable .= '<li><a class="social-link googleplus" href="'.$googleURL.'" target="_blank">Google+</a>';
		$variable .= '<li><a class="social-link pinterest" href="'.$pinterestURL.'" data-pin-custom="true" target="_blank">Pinterest</a>';
		$variable .= '<li><a class="social-link tumblr" href="'.$tumblrURL.'" target="_blank">Tumblr</a>';
		$variable .= '</ul>';
		$variable .= '</div>';
		
		return $variable;
	// }else{
	// 	// if not a post/page then don't include sharing button
	// 	return $variable;
	// }
};

?>