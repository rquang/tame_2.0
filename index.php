<?php get_header(); ?>

<?php while ( have_posts() ) : the_post(); ?>
    <article id="post-<?php the_ID(); ?>" <?php post_class($arr); ?>>
        <?php if ( has_post_thumbnail() && ! post_password_required() ) : ?>
        <div class="image">
            <?php the_post_thumbnail(); ?>
        </div>
        <?php endif; ?>
        <h1><?php the_title(); ?></h1>	
        <div class="inner-content">
            <?php the_content(); ?>
        </div>
    </article>
<?php endwhile; ?>

<?php get_footer(); ?>