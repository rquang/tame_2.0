<?php get_header(); ?>

<?php while ( have_posts() ) : the_post(); ?>
    <article id="post-<?php the_ID(); ?>" <?php post_class(array('single', 'feelings', 'typewriter')); ?>>
        <?php if ( has_post_thumbnail() && ! post_password_required() ) : ?>
        <div class="image">
            <?php the_post_thumbnail(); ?>
            <div class="typewriter-container" style="color: <?php echo get_post_meta( get_the_ID(), 'feelings_color', true ); ?>">
                <?php the_content(); ?>
            </div>
        </div>
        <?php endif; ?>
    </article>
<?php endwhile; ?>

<?php get_footer(); ?>