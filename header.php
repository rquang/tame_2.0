<!DOCTYPE html>
<!--[if lte IE 9]>      <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 9]><!--> <html <?php language_attributes(); ?> > <!--<![endif]-->
<head>
	<title><?php wp_title(''); ?> - <?php echo get_bloginfo( 'name' ); ?></title>
	<meta http-equiv="X-UA-Compatible" content="IE=11,chrome=1">
	<meta name="viewport" content="user-scalable=no, width=device-width, initial-scale=1, maximum-scale=1">
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="msapplication-TileColor" content="#000000">
	<meta name="theme-color" content="#000000">
	<meta name="google-site-verification" content="SwGsUsQxRG-oyZqyTkO2iy6JnWlXZM5f375_eH_WblQ" />
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

	<?php include_once("includes/google_analytics.php") ?>
	<?php wp_head(); ?>
	
</head>

<body <?php body_class(); ?>>

<?php include_once("includes/google_tag.php") ?>

<div id="border"></div>

<?php include_once("includes/preloader.php") ?>

<?php include_once("includes/navigation.php") ?>

<div id="page" class="site">

	<div id="content" class="site-content">

        <?php if (!is_cart() && ( is_page() || is_404() )) { ?>
            <a href="/" class="logo top flow">
                <img class="lazyload" src="<?php echo get_template_directory_uri(); ?>/images/tame-logo.svg" alt="Think About Me Ever Logo">
            </a>
        <?php } ?>

		<?php if ( is_woocommerce() || is_product_category() || is_page() || is_404() || is_singular('feelings') ) { ?>
			<div class="pink-bg"></div>
			<div class="flamez"></div>
		<?php } ?>


