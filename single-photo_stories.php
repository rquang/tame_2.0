<?php get_header(); ?>

<div class="sly-container is--single">
    <div class="frame">
        <ul class="slidee">

        <?php while ( have_posts() ) : the_post(); $bgcolor = get_post_meta( get_the_ID(), 'shoot_color', true ); ?>
            <li class="cell is--info" style="background-color:<?php echo $bgcolor ?>;">
                <div class="info-wrapper">
                    <div class="details">
                        <h1 class=><?php the_title(); ?></h1>
                        <div class="inner-details">
                            <ul class="label">
                                <li>Date /</li>
                                <?php 
                                    $models = get_post_meta( get_the_ID(), 'shoot_models', true );
                                    if (!empty($models)) {
                                        echo "<li>Model /</li>";
                                    }
                                ?>
                            </ul>
                            <ul class="data">
                                <li><?php echo get_post_meta( get_the_ID(), 'shoot_date', true ); ?></li>
                                <?php 
                                    $instagram = get_post_meta( get_the_ID(), 'shoot_models_insta', true );
                                    
                                    if (!empty($instagram)) {
                                        $result = [];
                                        $counter = 0;
                                        
                                        array_map(function($v1, $v2)use(&$result, &$counter){
                                            $result[!is_null($v1) ? $v1 : "filler" . $counter++] = !is_null($v2) ? $v2 : "filler";     
                                        }, $models, $instagram);
                                        
                                        foreach( (array) $result as $k => $v ) {
                                            if ($v == "filler")
                                                echo("<li>" . $k . "</li>");                                                
                                            else
                                                echo("<li>" . $k . "<a href='https://www.instagram.com/" . $v . "' target='_blank'>AA</a></li>");
                                        }
                                    } else {
                                        foreach( (array) $models as $model ) {
                                            echo("<li>" . $model . "</li>");
                                        }
                                    }

                                ?>
                            </ul>
                        </div>                    
                    </div>
                    <div class="description">
                        <?php the_content(); ?>
                    </div
                </div>					
            </li>
            <?php cmb2_output_file_list( 'shoot_gallery', 'full' ); ?>
            <li class="cell is--info is--last" style="background-color:<?php echo $bgcolor ?>;">
                <div class="info-wrapper">
                    <h2>
                        do you <span class="cycle-color">think about me ever</span>
                        <br>?
                    </h2>
                </div>
            </li>
            <li class="cell is--links">
                <?php cmb2_output_post2(get_previous_post(),'Prev'); ?>
                <?php cmb2_output_post2(get_next_post(),'Next'); ?>
            </li>
        <?php endwhile; ?>

        </ul>
    </div>
</div>        

<?php get_footer(); ?>

