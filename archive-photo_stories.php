<?php get_header(); ?>

<div class="sly-container is--archive">
    <div class="frame">
        <ul class="slidee">

        <?php $wpb_all_query = new WP_Query(array('post_type'=>'photo_stories', 'post_status'=>'publish', 'posts_per_page'=>-1)); ?>
        <?php if ( $wpb_all_query->have_posts() ) : ?>
            <?php while ( $wpb_all_query->have_posts() ) : $wpb_all_query->the_post(); ?>
                <li class="cell" style="background-color:<?php echo get_post_meta( get_the_ID(), 'shoot_color', true ); ?>;">
                    <?php if ( has_post_thumbnail() && ! post_password_required() ) : ?>
                        <div class="image">
                            <?php the_post_thumbnail('large',array( "class" => "lazyload")); ?>
                        </div>
                    <?php endif; ?>
                    <div class="details">
                        <a href="<?php echo esc_url( get_permalink() ); ?>"><h2 class="typewriter-container is--random"><?php the_title(); ?></h2></a>
                        <div class="inner-details">
                            <ul class="label">
                                <li>Date /</li>
                                <?php 
                                    $models = get_post_meta( get_the_ID(), 'shoot_models', true );
                                    if (!empty($models)) {
                                        echo "<li>Model /</li>";
                                    }
                                ?>
                            </ul>
                            <ul class="data">
                                <li><?php echo get_post_meta( get_the_ID(), 'shoot_date', true ); ?></li>
                                <?php 
                                    foreach( (array) $models as $model ) {
                                        echo("<li>" . $model . "</li>");
                                    }
                                ?>
                            </ul>
                        </div>
                        <a href="<?php echo esc_url( get_permalink() ); ?>" class="btn-read primary-btn">See Story</a>   
                    </div>
                </li>
            <?php endwhile; ?>
        <?php endif; ?>
        </ul>
    </div>
</div>        

<?php get_footer(); ?>