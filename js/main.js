(function ($) {

    $(document).ready(function ($) {

        var sly;

        $(window).on("load", function () {
            if ($('.sly-container').length && !$('.sly-container').hasClass('is--disabled')) {
                slySlider();
                $('.cell').each(function () {
                    isVisible($(this));
                });
                if ($('.single').length) {
                    cellNumber('.sly-container');
                }
            }
            if (!sessionStorage.viewed) {
                setTimeout(function () {
                    $('#border').addClass("is--ready is--visited");
                }, 100);
                setTimeout(function () {
                    $('#page').addClass("is--ready");
                }, 400);
                sessionStorage.viewed = 1;
            } else {
                $('#border').addClass("is--visited");
                $('#page').addClass("is--ready");
            }
            // $(".preloader-container").fadeOut(function () {
            //     $(this).remove();
            // });
            $(".preloader-container").fadeOut();
        });

        $('.btn-nav').on('click', function () {
            if (!$('nav').hasClass('is--active')) {
                $('nav').addClass('is--active');
            } else {
                $('nav').removeClass('is--active');
            }
        });

        if ($('body').hasClass('mobile')) {
            $(window).bind('scroll', function () {
                scrollVisible();
            });

            $('body.home .cell a').on('click', function () {
                $this = $(this);
                if (!$this.hasClass('is--active')) {
                    $('body.home .cell a').removeClass('is--active');
                    $this.addClass('is--active');
                    return false;
                }
            });
        }

        if ($('.typewriter-container').length) {
            typeWriter();
        }

        if ($('.single-product').length) {
            productViewer();
        }

        $('#random .primary-btn').on('click', function (e) {
            e.preventDefault();

            var $container = $('.sly-container .slidee');
            var $this = $(this);

            $this.parent().removeClass('bounceFade');
            $this.prop('disabled', true);
            $this.html('loading');

            $(".preloader-container").css('z-index', 2).fadeIn();

            // $container.html('<li class="is--loading"><div class="wave"></div></li>');

            $.ajax({
                    url: woocommerce_params.ajax_url,
                    type: 'GET',
                    data: {
                        action: 'random_photo_stories_ajax',
                    },
                    dataType: 'html'
                })
                .success(function (results) {
                    $container.html(results).imagesLoaded().then(function(){
                        typeWriter();
                        $this.parent().addClass('bounceFade');
                        $this.prop('disabled', false);
                        $this.html('touch me');
                        $(".preloader-container").fadeOut();
                    });
                })
                .fail(function (xhr, err) {
                    console.log("readyState: " + xhr.readyState + "\nstatus: " + xhr.status);
                    console.log("responseText: " + xhr.responseText);
                });
        });

        // $('.woocommerce-message, .woocommerce-error').on('click', '::after' , function () {
        //     $(this).fadeOut();
        //     console.log('yes');
        // });
    });

    function productViewer() {
        var $imageContainer = $('.woocommerce-product-gallery--with-images'),
            $images = $imageContainer.find('figure').clone();
        $images.removeClass().addClass('woocommerce-product-gallery-quickview__wrapper');
        $imageContainer.append($images);

        $images.on('click', 'a', function (e) {
            e.preventDefault();
            var $figureContainer = $imageContainer.find('figure:first-child'),
                $index = $(this).parent().index(),
                scrollTo = $figureContainer.find('div').get($index);
            if ($images.outerHeight() < $images.outerWidth()) {
                $imageContainer.animate({
                    scrollLeft: $(scrollTo).offset().left - $imageContainer.offset().left + $imageContainer.scrollLeft()
                });
            } else {
                $imageContainer.animate({
                    scrollTop: $(scrollTo).offset().top - $imageContainer.offset().top + $imageContainer.scrollTop()
                });
            }
        });
    }

    function slySlider() {
        var $frame = $('.sly-container .frame');
        var $wrap = $frame.parent();
        var $controls = $('#controls');
        var options = {
            horizontal: 1,
            itemNav: 'centered',
            smart: 1,
            activateMiddle: 0,
            mouseDragging: 1,
            touchDragging: 1,
            releaseSwing: 1,
            startAt: 0,
            scrollBar: $('#scrollbar'),
            scrollBy: 1,
            scrollHijack: 300,
            scrollTrap: 1,
            speed: 300,
            elasticBounds: 1,
            easing: 'easeOutExpo',
            dragHandle: 1,
            dynamicHandle: 1,
            clickBar: 1,
            keyboardNavBy: 'items',
            activeClass: 'is--active',
            disabledClass: 'is--disabled',
            cycleBy: 'items',
            cycleInterval: 1500,
            startPaused: 1,
            slidebyone: true,
            prev: $controls.find('.btn-prev'),
            next: $controls.find('.btn-next')
        };
        var callbacks = {
            move: function () {
                $('.cell').each(function () {
                    isVisible($(this));
                });
            }
        };
        var initFlag = true;

        sly = new Sly($frame, options, callbacks);

        orientationUpdate();
        init();

        function orientationUpdate() {
            initFlag = true;

            if ($('body').hasClass('page') || $('body').hasClass('woocommerce') || $('body').hasClass('mobile') || $('body').hasClass('error404')) {
                initFlag = false;
            } else {
                if (getOrientation() == 90) {
                    sly.set({
                        horizontal: 1,
                        itemNav: 'centered',
                        activateMiddle: 0
                    });
                    $('#scrollbar').removeClass('right').addClass('bottom');
                } else {
                    sly.set({
                        horizontal: 0,
                        itemNav: 'forceCentered',
                        activateMiddle: 1
                    });
                    $('#scrollbar').removeClass('bottom').addClass('right');
                }
            }
        }

        function init() {
            sly.destroy();
            if (initFlag) {
                $('body').addClass('sly');
                sly.init();
            } else {
                $('body').removeClass('sly');
                $frame.find('.slidee').removeAttr("style");
            }
        }

        // To Start button
        $controls.find('.btn-toStart').on('click', function () {
            var item = $(this).data('item');
            sly.activate(0);
            sly.toStart(item);
        });
        // To End button
        $controls.find('.btn-toEnd').on('click', function () {
            var item = $(this).data('item');
            var total = $('.cell').length;
            sly.activate(total - 1);
            sly.toEnd(item);
        });
        // Toggle button
        $controls.find('.btn-autoPlay').on('click', function () {
            var toggle = $(this).find('span');
            if (toggle.html() == 'on')
                toggle.html('off');
            else
                toggle.html('on');
            $(this).toggleClass('is--active');
            sly.toggle();
        });

        $(window).on('resize', function (event) {
            orientationUpdate();
            init();
        });
    }

    function scrollVisible() {
        var lastScroll = 0;
        var scroll = $(this).scrollTop();

        if (scroll > lastScroll) {
            $('.cell').each(function () {
                isVisible($(this));
            });
        }
        lastScroll = scroll;
    }

    function getOrientation() {
        var orientation = window.innerWidth > window.innerHeight ? "90" : "0";
        return orientation;
    }

    function isVisible(id) {
        if ($(id).visible(true)) {
            $(id).addClass('is--visible');
        }
    }

    function getRandomInt(min, max) {
        return Math.floor(Math.random() * (max - min + 1)) + min;
    }

    function cellNumber(elem) {
        $(elem).find('.cell').not('.is--info, .is--last, .is--links').each(function () {
            var index = $(this).index();
            $(this).append("<span class='index'>" + index + "</span>");
        });
    }

    function typeWriter() {
        $('.typewriter-container').each(function () {
            var app = this;
            var text = $(app).text();
            var tSpeed = (($(app).hasClass('is--random')) ? getRandomInt(50, 150) : 50);
            var dSpeed = (($(app).hasClass('is--random')) ? getRandomInt(50, 150) : 25);

            var xArr = [
                'is--left',
                'is--center',
                'is--right'
            ];

            var yArr = [
                'is--top',
                'is--middle',
                'is--bottom'
            ];

            var sizeArr = [
                'is--small',
                'is--medium',
                'is--big'
            ];

            var typewriter = new Typewriter(app, {
                loop: true,
                typingSpeed: tSpeed,
                deleteSpeed: dSpeed,
                animateCursor: false
            });

            // $(app).css({
            //     "display" : "block",
            //     "min-height" : $(app).height()
            // });

            if ($('body').hasClass('home')) {
                $(this).find('.typewriter-wrapper')
                    .addClass(xArr[getRandomInt(0, 2)])
                    .addClass(yArr[getRandomInt(0, 2)])
                    .addClass(sizeArr[getRandomInt(0, 2)]);
            }

            typewriter.typeString(text)
                .pauseFor(2000)
                .deleteAll()
                .start();
        });
    }

    $.fn.imagesLoaded = function () {

        // get all the images (excluding those with no src attribute)
        var $imgs = this.find('img[src!=""]');
        // if there's no images, just return an already resolved promise
        if (!$imgs.length) {
            return $.Deferred().resolve().promise();
        }

        // for each image, add a deferred object to the array which resolves when the image is loaded (or if loading fails)
        var dfds = [];
        $imgs.each(function () {

            var dfd = $.Deferred();
            dfds.push(dfd);
            var img = new Image();
            img.onload = function () {
                dfd.resolve();
            };
            img.onerror = function () {
                dfd.resolve();
            };
            img.src = this.src;

        });

        // return a master promise object which will resolve when all the deferred objects have resolved
        // IE - when all the images are loaded
        return $.when.apply($, dfds);

    };

})(jQuery);