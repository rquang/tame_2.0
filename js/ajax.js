(function ($) {

    function bagMessage(text) {
        var $this = $('#btn-cart');
        $this.addClass('bounceFade');
        setTimeout(function () {
            $this.removeClass('bounceFade');
        }, 3000);
        // var $this = $('.cart-message');
        // $this.html(text);
        // setTimeout(function () {
        //     $this.empty();
        // }, 3000);
    }

    // Ajax add to cart on the product page
    var $warp_fragment_refresh = {
        url: wc_cart_fragments_params.wc_ajax_url.toString().replace('%%endpoint%%', 'get_refreshed_fragments'),
        type: 'POST',
        success: function (data) {
            if (data && data.fragments) {

                $.each(data.fragments, function (key, value) {
                    $(key).replaceWith(value);
                });

                $(document.body).trigger('wc_fragments_refreshed');

                var $cart = $('#btn-cart');
                var $button = $('.single_add_to_cart_button');
                $button.html('added!');
                $cart.addClass('bounceFade');

                setTimeout(function () {
                    $cart.removeClass('bounceFade');
                    $button.html('add to cart');
                }, 3000);

                bagMessage('added to bag');

            }
        }
    };

    $('.entry-summary form.cart').on('submit', function (e) {
        e.preventDefault();

        // $('.entry-summary').block({
        //     // message: '<img src="/wp/wp-content/themes/tame/images/loader.gif"/>',
        //     message: null,
        //     overlayCSS: {
        //         backgroundColor: '#fff',
        //         cursor: 'none'
        //     }
        // });

        $('.single_add_to_cart_button').prop('disabled', true);
        $('.single_add_to_cart_button').html('loading');

        var product_url = window.location,
            form = $(this);

        $.post(product_url, form.serialize() + '&_wp_http_referer=' + product_url, function (result) {
            var cart_dropdown = $('.widget_shopping_cart', result);

            // update dropdown cart
            $('.widget_shopping_cart').replaceWith(cart_dropdown);

            // update fragments
            $.ajax($warp_fragment_refresh);

            // $('.entry-summary').unblock();

            $('.single_add_to_cart_button').prop('disabled', false);


        });
    });

})(jQuery);