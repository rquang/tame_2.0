<nav id="nav" class="fixed top">

    <?php if ( ( !is_woocommerce() && !is_product_category() && !is_page() && !is_404() ) ) : ?>

        <a href="/" class="logo top">
            <img class="lazyload" src="<?php echo get_template_directory_uri(); ?>/images/tame-logo.svg" alt="Think About Me Ever Logo">
        </a>

    <?php endif; ?>
        
    <?php wp_nav_menu( array('menu' => 'main')); ?>

    <?php if ( in_array( 'woocommerce/woocommerce.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) ) {       
        $count = WC()->cart->cart_contents_count; ?>
        <div id="btn-cart">
            <a class="cart-contents" href="/cart/" title="<?php _e( 'View your shopping cart' ); ?>">
                <span class="label">shopping bag</span>
                <span class="icon">
                    <?php if ( $count > 0 ) { ?>         
                        <span class="cart-contents-count"><?php echo esc_html( $count ); ?></span>
                    <?php } ?>
                </span>
            </a>
        </div>   
    <?php } ?>

    <span class="btn-nav"></span>
</nav>

<?php echo social_sharing_buttons() ?>

<div id="collab" class="fixed right">
    <a href="/contact" class="btn-collab cycle-color">let's collaborate</a>
</div>

<?php if ( is_singular('photo_stories') ) : ?>

    <div id="controls" class="fixed bottom">
        <div class="btn-group">
            <button class="btn btn-toStart">start</button>
            <button class="btn btn-prev">prev</button>
            <span class="separator"><b>/</b></span>
            <button class="btn btn-toggle btn-autoPlay"><label>autoplay</label>:<span>off</span></button>
            <span class="separator"><b>/</b></span>
            <button class="btn btn-next">next</button>
            <button class="btn btn-toEnd">end</button>
        </div>
    </div>

<?php endif; if ( is_singular('photo_stories') || is_archive('photo_stories') || is_front_page() ) : ?>

    <div id="scrollbar" class="fixed">
        <div class="handle"></div>
    </div>

<?php endif; if ( is_home() ) : ?>

    <div id="random" class="fixed bottom">
        <div class="btn-wrapper bounceFade">
            <button class="primary-btn">TOUCH ME</button>
        </div>
    </div>

<?php endif; ?>
