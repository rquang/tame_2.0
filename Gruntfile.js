module.exports = function(grunt) {

    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-jshint');
    grunt.loadNpmTasks('grunt-sass');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-notify');
    grunt.loadNpmTasks('grunt-postcss');
    grunt.loadNpmTasks('grunt-browser-sync');

    grunt.initConfig({
        // path: 'localhost/htdocs/WP/wp-content/themes/gucci/',
        pkg: grunt.file.readJSON('package.json'),
        postcss: {
            options: {
                map: false,
                processors: [
                    require('pixrem')(),
                    require('autoprefixer')({
                        browsers: 'last 2 versions'
                    }),
                    require('cssnano')()
                ],
            },
            dist: {
                expand: true,
                flatten: true,
                src: 'style.css'
            }
        },
        uglify: {
            files: {
                src: 'js/*.js',
                dest: 'js/min/',
                expand: true,
                flatten: true,
                ext: '.min.js'
            }
        },
        jshint: {
            all: ['Gruntfile.js', 'js/*.js'],
            options: {
                expr: true,
                boss: true,
                loopfunc: true,
                sub: true,
                newcap: false,
            }
        },
        notify: {
            sass: {
                options: {
                    title: "CSS Files Compiled",
                    message: "SASS task complete"
                }
            }
        },
        sass: {
            dist: {
                files: {
                    "style.css": "scss/main.scss"
                }
            }
        },
        browserSync: {
            dev: {
                // bsFiles: {
                //     src: [
                //         '<%= path %>style.css',
                //         '<%= path %>js/**/*.js',
                //         '<%= path %>**/*.php'
                //     ]
                // },
                options: {
                    files: ['style.css', 'js/**/*.js', '**/*.php'],
                    watchTask: true,
                    // debugInfoe: true,
                    // logConnections: true,
                    // notify: true,
                    proxy: "thinkaboutmeever.dev",
                    // tunnel: "external",
                    host: '192.168.0.101'
                }
            }
        },
        watch: {
            gruntfile: {
                files: 'Gruntfile.js',
                tasks: ['jshint'],
            },
            markup: {
                files: ['*.php', '*/**/*.php'],
                // options: {
                //     livereload: true,
                // }
            },
            scripts: {
                files: ['js/*.js'],
                tasks: ['jshint'],
                options: {
                    spawn: true,
                    // livereload: true,
                },
            },
            styles: {
                files: ['scss/**'],
                tasks: ['sass', 'notify:sass'],
                // tasks: ['sass', 'notify:sass', 'postcss'],
                // options: {
                //     compress: true,
                //     // livereload: true,
                // }
            },

        }
    });

    grunt.registerTask('production', ['uglify', 'jshint', 'sass', 'postcss']);
    grunt.registerTask('default', ['browserSync', 'watch']);
};
