<?php get_header(); ?>

<?php while ( have_posts() ) : the_post(); ?>
    <article id="post-<?php the_ID(); ?>" <?php post_class(array('single', 'contact', 'typewriter')); ?>>
        <div class="typewriter-container">
            <?php the_content(); ?>
        </div>
        <div class="content">
            <a class="title" href="mailto:feelings@thinkaboutmeever.com">feelings@thinkaboutmeever.com</a>
            <?php echo do_shortcode('[contact-form-7 id="402" title="Contact"]'); ?>
        </div>
    </article>
<?php endwhile; ?>

<?php get_footer(); ?>